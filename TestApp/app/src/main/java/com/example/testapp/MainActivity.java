package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;



public class MainActivity extends AppCompatActivity {













    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);












        //Background Code
        /*
        Intent serviceIntent = new Intent(this, MyBackgService.class);
        startService(serviceIntent);
        */











        //Foreground Code
        /*
        Intent serviceIntent = new Intent(this, MyForegService.class);
        startForegroundService(serviceIntent);
        */








        //Foreground Code with Broad Cast Receiver

        if(!foregroundServiceRunning()) {
            Intent serviceIntent = new Intent(this, MyForegService.class);
            startForegroundService(serviceIntent);
        }





    }



    public boolean foregroundServiceRunning(){
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for(ActivityManager.RunningServiceInfo service: activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if(MyForegService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }



}