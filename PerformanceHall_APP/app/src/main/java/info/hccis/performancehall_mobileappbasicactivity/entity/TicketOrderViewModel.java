package info.hccis.performancehall_mobileappbasicactivity.entity;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class TicketOrderViewModel extends ViewModel {

    private List<TicketOrder> ticketOrders = new ArrayList();

    public List<TicketOrder> getTicketOrders() {
        return ticketOrders;
    }

    public void setTicketOrders(List<TicketOrder> ticketOrders) {
        this.ticketOrders.clear();
        this.ticketOrders.addAll(ticketOrders);
    }
}
